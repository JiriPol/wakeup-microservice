﻿using Microsoft.Extensions.Logging;
using System;
using JiriPol.Utilities.Wakeup.Extensions;

namespace JiriPol.Utilities.Wakeup.Common
{
    public class Guard
    {
        public static void ArgumentNotNull(string argumentName, object value, ILogger logger = null)
        {
            if (value == null)
            {
                string exceptionMessage = $"Value {argumentName} cannot be null.";

                ArgumentNullException argumentNullException = new ArgumentNullException(exceptionMessage);
                if (logger != null && logger.IsErrorEnabled())
                {
                    logger.LogError(argumentNullException, exceptionMessage);
                }

                throw argumentNullException;
            }
        }

        public static void ArgumentNotNullOrEmpty(string argumentName, string value, ILogger logger = null)
        {
            ArgumentNotNull(argumentName, value, logger);
            if (value.Length == 0)
            {
                string exceptionMessage = $"Value {argumentName} cannot be an empty string.";

                ArgumentException argumentException = new ArgumentException(exceptionMessage);
                if (logger != null && logger.IsErrorEnabled())
                {
                    logger.LogError(argumentException, exceptionMessage);
                }

                throw argumentException;
            }
        }

        public static void ArgumentValid(string argumentName, bool valid, string exceptionMessage, ILogger logger = null)
        {
            if (!valid)
            {
                ArgumentException argumentException = new ArgumentException(exceptionMessage, argumentName);
                if (logger != null && logger.IsErrorEnabled())
                {
                    logger.LogError(argumentException, exceptionMessage);
                }

                throw argumentException;
            }
        }

        public static void OperationValid(bool valid, string exceptionMessage, ILogger logger = null)
        {
            if (!valid)
            {
                InvalidOperationException invalidOperationException = new InvalidOperationException(exceptionMessage);
                if (logger != null && logger.IsErrorEnabled())
                {
                    logger.LogError(invalidOperationException, exceptionMessage);
                }

                throw invalidOperationException;
            }
        }
    }
}
