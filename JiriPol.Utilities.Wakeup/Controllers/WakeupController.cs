﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using JiriPol.Utilities.Wakeup.Repository;
using Microsoft.Extensions.Configuration;
using JiriPol.Utilities.Wakeup.Common;
using JiriPol.Utilities.Wakeup.Lan;
using Microsoft.Extensions.Logging;
using JiriPol.Utilities.Wakeup.Repositories;
using JiriPol.Utilities.Wakeup.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JiriPol.Utilities.Wakeup.Controllers
{
    [Route("api/[controller]/v1")]
    public class WakeupController : Controller
    {
        private readonly ILogger m_logger;

        private readonly IConfiguration m_configuration;
        private readonly IMemoryCache m_cache;

        public WakeupController(IConfiguration configuration, IMemoryCache memoryCache, ILogger<WakeupController> logger) {
            m_cache = memoryCache;
            m_configuration = configuration;

            m_logger = logger;
        }

        //List adapters - DONE
        [HttpGet]
        [Route("adapter")]
        public async Task<IQueryable<AdapterEntity>> Get()
        {
            AdapterRepository adapterRepository =
                await m_cache.GetOrCreateAsync<AdapterRepository>("AdapterRepository", async entry =>
                {
                    entry.SetAbsoluteExpiration(TimeSpan.FromSeconds(60));

                    string filePath = m_configuration.GetValue<string>("AdapterStorageFile");
                    AdapterRepository adapters = await AdapterRepository.CreateAsync(filePath);

                    return adapters;
                });
           
            return adapterRepository.GetAll();
        }

        //Wakeup adapter
        [HttpGet]
        [Route("adapter/{adapterId}/wake")]
        public async Task<IActionResult> GetAsync(string adapterId, [FromQuery] string userName)
        {
            Guard.ArgumentNotNullOrEmpty(nameof(adapterId), adapterId);

            AdapterRepository adapterRepository =
                await m_cache.GetOrCreateAsync<AdapterRepository>("AdapterRepository", async entry =>
                {
                    entry.SetAbsoluteExpiration(TimeSpan.FromSeconds(60));

                    string filePath = m_configuration.GetValue<string>("AdapterStorageFile");
                    AdapterRepository adapters = await AdapterRepository.CreateAsync(filePath);

                    return adapters;
                });

            AdapterEntity adapter = adapterRepository.GetSingle(adapterId);

            WakeOnLan wakeOnLan = new WakeOnLan();
            await wakeOnLan.WakeUpAsync(adapter.AdapterMacAddress, m_configuration.GetValue<string>("NetworkBroadcastAddress"));

            return Json(new JiriPol.Utilities.Wakeup.Repository.Response()
            {
                Status = new Status() { CodeName = "wake up", Message = "Adapter successfuly notified." },
                Adapter = adapter
            });
        }

        //Register adapter
        [HttpPost]
        [Route("adapter")]
        public async Task<IActionResult> PostAsync([FromBody]AdapterEntity adapter)
        {
            Guard.ArgumentNotNullOrEmpty(nameof(adapter.AdapterMacAddress), adapter.AdapterMacAddress);
            Guard.ArgumentNotNullOrEmpty(nameof(adapter.AdapterName), adapter.AdapterName);

            AdapterRepository adapterRepository =
                await m_cache.GetOrCreateAsync<AdapterRepository>("AdapterRepository", async entry =>
                {
                    entry.SetAbsoluteExpiration(TimeSpan.FromSeconds(60));

                    string filePath = m_configuration.GetValue<string>("AdapterStorageFile");
                    AdapterRepository adapters = await AdapterRepository.CreateAsync(filePath);

                    return adapters;
                });

            adapter.AdapterId = Guid.NewGuid().ToString();

            adapterRepository.Add(adapter);

            await adapterRepository.SaveAsync();

            m_cache.Remove("AdapterRepository");

            return Json(new JiriPol.Utilities.Wakeup.Repository.Response()
            {
                Status = new Status() { CodeName = "created", Message = "Adapter successfuly created." },
                Adapter = adapter
            });
        }

        //Remove adapter
        [HttpDelete]
        [Route("adapter/{adapterId}")]
        public async Task<IActionResult> DeleteAsync(string adapterId)
        {
            Guard.ArgumentNotNullOrEmpty(nameof(adapterId), adapterId);

            AdapterRepository adapterRepository =
                await m_cache.GetOrCreateAsync<AdapterRepository>("AdapterRepository", async entry =>
                {
                    entry.SetAbsoluteExpiration(TimeSpan.FromSeconds(60));

                    string filePath = m_configuration.GetValue<string>("AdapterStorageFile");
                    AdapterRepository adapters = await AdapterRepository.CreateAsync(filePath);

                    return adapters;
                });


            AdapterEntity deletedAdapterEntity = adapterRepository.Delete(adapterId);
            if (deletedAdapterEntity != null)
            {
                await adapterRepository.SaveAsync();
                m_cache.Remove("AdapterRepository");

                Response.StatusCode = 200;
                return Json(new JiriPol.Utilities.Wakeup.Repository.Response()
                {
                    Status = new Status() { CodeName = "deleted", Message = "Adapter successfuly deleted." },
                    Adapter = deletedAdapterEntity
                });
            }
            else
            {
                Response.StatusCode = 404;
                return Json(new JiriPol.Utilities.Wakeup.Repository.Response()
                {
                    Status = new Status() { CodeName = "not deleted", Message = "Adapter does not exist." },
                });
            }
        }
    }
}
