﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JiriPol.Utilities.Wakeup.Entities
{
    public class AdapterEntity
    {
        public string AdapterMacAddress { get; set; }
        public string AdapterName { get; set; }
        public string AdapterId { get; set; }
    }
}
