﻿using JiriPol.Utilities.Wakeup.Entities;
using System.Collections.Generic;

namespace JiriPol.Utilities.Wakeup.Repository
{
    public class RegisteredAdapters
    {
        public IList<AdapterEntity> Adapters { get; set; }
    }
}
