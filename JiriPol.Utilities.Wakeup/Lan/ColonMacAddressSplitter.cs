﻿
namespace JiriPol.Utilities.Wakeup.Lan
{
    public class ColonMacAddressSplitter : IMacAddressSplitter
    {
        public string[] Split(string macAddress)
        {
            return macAddress.Split(':');
        }
    }
}
