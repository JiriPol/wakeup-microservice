﻿namespace JiriPol.Utilities.Wakeup.Lan
{
    public class DashMacAddressSplitter : IMacAddressSplitter
    {
        public string[] Split(string macAddress)
        {
            return macAddress.Split('-');
        }
    }
}
