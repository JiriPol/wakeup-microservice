﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace JiriPol.Utilities.Wakeup.Lan
{
    public class WakeOnLan
    {
        private string[] SplitMacToDigits(string macAddress) {
            if (macAddress.Contains("-"))
            {
                return new DashMacAddressSplitter().Split(macAddress);
            }
            if (macAddress.Contains(":"))
            {
                return new ColonMacAddressSplitter().Split(macAddress);
            }

            return new string[0];
        }

        public async Task WakeUpAsync(string macAddress, string broadcastAddress)
        {
            UdpClient client = new UdpClient();

            Byte[] datagram = new byte[102];

            for (int i = 0; i <= 5; i++)
            {
                datagram[i] = 0xff;
            }

            string[] macDigits = SplitMacToDigits(macAddress);

            if (macDigits.Length != 6)
            {
                throw new ArgumentException("Incorrect MAC address supplied!");
            }

            int start = 6;
            for (int i = 0; i < 16; i++)
            {
                for (int x = 0; x < 6; x++)
                {
                    datagram[start + i * 6 + x] = (byte)Convert.ToInt32(macDigits[x], 16);
                }
            }

            IPAddress broadcast = IPAddress.Parse(broadcastAddress);

            await client.SendAsync(datagram, datagram.Length, broadcastAddress.ToString(), 3);
        }
    }
}
