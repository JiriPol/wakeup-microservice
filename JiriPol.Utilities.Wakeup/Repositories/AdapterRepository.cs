﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JiriPol.Utilities.Wakeup.Entities;
using JiriPol.Utilities.Wakeup.Repository;
using JiriPol.Utilities.Wakeup.Storage;

namespace JiriPol.Utilities.Wakeup.Repositories
{
    public class AdapterRepository : IAdapterRepository
    {
        private RegisteredAdapters m_registeredAdapters;
        private readonly string m_filePath;

        public static Task<AdapterRepository> CreateAsync(string filePath)
        {
            AdapterRepository adapterRepository = new AdapterRepository(filePath);

            return adapterRepository.InitializeAsync();
        }

        private AdapterRepository(string filePath)
        {
            m_filePath = filePath;
        }

        private async Task<AdapterRepository> InitializeAsync()
        {
            m_registeredAdapters = await LoadRegisteredAdapters();

            return this;
        }

        private async Task<RegisteredAdapters> LoadRegisteredAdapters()
        {
            JsonFileReader jsonFileReader = new JsonFileReader(m_filePath);

            return await jsonFileReader.ReadAsync();
        }      

        public void Add(AdapterEntity adapterEntity)
        {
            m_registeredAdapters.Adapters.Add(adapterEntity);
        }

        public int Count()
        {
            return m_registeredAdapters.Adapters.Count;
        }

        public AdapterEntity Delete(string adapterId)
        {
            AdapterEntity adapter = m_registeredAdapters.Adapters.
                FirstOrDefault<AdapterEntity>(x => x.AdapterId == adapterId);

            m_registeredAdapters.Adapters.Remove(adapter);

            return adapter;
        }

        public IQueryable<AdapterEntity> GetAll()
        {
            return m_registeredAdapters.Adapters.AsQueryable();
        }

        public AdapterEntity GetSingle(string adapterId)
        {
            return m_registeredAdapters.Adapters.FirstOrDefault(x => x.AdapterId == adapterId);
        }

        public async Task<bool> SaveAsync()
        {
            JsonFileWriter jsonFileWriter = new JsonFileWriter(m_filePath);
            try
            {
                await jsonFileWriter.WriteAsync(m_registeredAdapters);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public void Update(AdapterEntity adapterEntity)
        {
            Delete(adapterEntity.AdapterId);

            m_registeredAdapters.Adapters.Add(adapterEntity);
        }
    }
}
