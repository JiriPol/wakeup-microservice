﻿using JiriPol.Utilities.Wakeup.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace JiriPol.Utilities.Wakeup.Repositories
{
    interface IAdapterRepository
    {
        void Add(AdapterEntity adapterEntity);
        AdapterEntity Delete(string adapterId);
        IQueryable<AdapterEntity> GetAll();
        AdapterEntity GetSingle(string adapterId);
        Task<bool> SaveAsync();
        int Count();
        void Update(AdapterEntity adapterEntity);
    }
}
