﻿
using JiriPol.Utilities.Wakeup.Entities;

namespace JiriPol.Utilities.Wakeup.Repository
{
    public class Response
    {
        public Status Status { get; set; }

        public AdapterEntity Adapter { get; set; }
    }
}
