﻿namespace JiriPol.Utilities.Wakeup.Repository
{
    public class Status
    {
        public string CodeName { get; set; }
        public string Message { get; set; }
    }
}
