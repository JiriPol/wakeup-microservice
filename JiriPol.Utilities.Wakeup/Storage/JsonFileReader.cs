﻿using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;
using JiriPol.Utilities.Wakeup.Common;
using JiriPol.Utilities.Wakeup.Repository;

namespace JiriPol.Utilities.Wakeup.Storage
{
    public class JsonFileReader
    {
        private readonly string m_filePath;

        public JsonFileReader(string filePath) {
            Guard.ArgumentNotNullOrEmpty(nameof(filePath), filePath);
            m_filePath = filePath;

            Guard.ArgumentValid(nameof(filePath), File.Exists(filePath), "Specified file does not exist.");
        }

        public async Task<RegisteredAdapters> ReadAsync() {
            using (StreamReader file = File.OpenText(m_filePath)) {
                JsonSerializer jsonSerializer = new JsonSerializer();
                return await Task.Run(() => (RegisteredAdapters)jsonSerializer.Deserialize(file, typeof(RegisteredAdapters)));
            }
        }
    }
}
