﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JiriPol.Utilities.Wakeup.Common;
using JiriPol.Utilities.Wakeup.Repository;

namespace JiriPol.Utilities.Wakeup.Storage
{
    public class JsonFileWriter
    {
        private readonly string m_filePath;

        public JsonFileWriter(string filePath)
        {
            Guard.ArgumentNotNullOrEmpty(nameof(filePath), filePath);
            m_filePath = filePath;

            Guard.ArgumentValid(nameof(filePath), File.Exists(filePath), "Specified file does not exist.");
        }

        public async Task WriteAsync(RegisteredAdapters registeredAdapters)
        {
            using (StreamWriter file = File.CreateText(m_filePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                await Task.Run(() => serializer.Serialize(file, registeredAdapters));
            }
        }

    }
}
